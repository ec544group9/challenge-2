/*
 * TemperatureSensor.java
 *
 * Copyright (c) 2008-2010 Sun Microsystems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

package challenge2.spot;

import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.util.Utils;
import javax.microedition.io.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.resources.transducers.IMeasurementInfo;

/**
 * This application is the 'on SPOT' portion of the SendDataDemo. It
 * periodically samples a sensor value on the SPOT and transmits it to
 * a desktop application (the 'on Desktop' portion of the SendDataDemo)
 * where the values are displayed.
 *
 * @author: Vipul Gupta
 * modified: Ron Goldman
 *
 * This program was heavily modified for the Challenge 1 in EC544
 * @author: Group 9
 */
public class TemperatureSensor extends MIDlet 
{
    private static final int HOST_PORT = 57;
    private static final int SEND_PERIOD = 5 * 1000; // Send out an average every 5 seconds
    private static final int SAMPLE_PERIOD = 500;  // in milliseconds (every half second)
    
    /* The running sum for the average value */
    private float runningSumC = 0.0f;
    private float runningSumF = 0.0f;
    private final int totalCount = SEND_PERIOD / SAMPLE_PERIOD; // This must be a whole number to be accurate and make sense
    private ITemperatureInput tempSensor = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
    
    protected void startApp() throws MIDletStateChangeException {
        RadiogramConnection rCon = null;
        Datagram dg = null;
        String ourAddress = System.getProperty("IEEE_ADDRESS");
        ILightSensor lightSensor = (ILightSensor)Resources.lookup(ILightSensor.class);
        ITriColorLED led = (ITriColorLED)Resources.lookup(ITriColorLED.class, "LED7");
        
        System.out.println("Starting sensor sampler application on " + ourAddress + " ...");

        // Listen for downloads/commands over USB connection
        new com.sun.spot.service.BootloaderListenerService().getInstance().start();

        try {
            // Open up a broadcast connection to the host port
            // where the 'on Desktop' portion of this demo is listening
            rCon = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT);
            dg = rCon.newDatagram(50);  // only sending 12 bytes of data
        } catch (Exception e) {
            System.err.println("Caught " + e + " in connection initialization.");
            notifyDestroyed();
        }
        
        /* Outer loop is the message sending loop */
        while (true) {
            /* Inner loop is the individual readings */
            for (int ii = 0 ; ii < totalCount ; ii++)
            {
                try {
                    double tempC = tempSensor.getCelsius();      // Temperature in Celcius.
                    double tempF = tempSensor.getFahrenheit();   // Temperature in Farenheight

                    // Get the current time and sensor reading
                    long now = System.currentTimeMillis();
                    
                    System.out.println("time: " + now + "  temperature: " + tempC  + " C = " + tempF  + " F");
                    
                    // Flash an LED to indicate a sampling event
                    led.setRGB(255, 255, 255);
                    led.setOn();
                    Utils.sleep(50);
                    led.setOff();

                    runningSumC += tempC;
                    runningSumF += tempF;
                    
                    // Go to sleep to conserve battery
                    Utils.sleep(SAMPLE_PERIOD - (System.currentTimeMillis() - now));
                } catch (Exception e) {
                    System.err.println("Caught " + e + " while collecting/sending sensor sample.");
                }
            }
            
            /* Calculate the average temperature */
            float avgC = runningSumC / totalCount;
            float avgF = runningSumF / totalCount;
            long sendTime = System.currentTimeMillis();
            
            System.out.println("Average of " + totalCount + " temperatures over " + SEND_PERIOD + " ms is " + avgC  + " C and " + avgF + " F.");
            
            // Package the time and sensor reading into a radio datagram and send it.
            try
            {
                dg.reset();
                dg.writeLong(sendTime);
                dg.writeDouble(avgF);
                dg.writeDouble(avgC);
                rCon.send(dg);
            }
            catch (Exception e)
            {
                System.err.println("Caught " + e + " while attempting to send datagram.");
            }
            
            /* Reset the variables for the averages */
            runningSumC = 0;
            runningSumF = 0;
        }
    }
    
    protected void pauseApp() {
        // This will never be called by the Squawk VM
    }
    
    protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
        // Only called if startApp throws any exception other than MIDletStateChangeException
    }
}
