'''
Created on Sep 19, 2013

Simple data storage class for the sensor data.  Stores three
arrays coinciding to time, celsius, and fahrenheit for the readings.
Provides methods to easily retrieve the desired array

@author: soptnrs
'''

class SensorData(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.time = []
        self.celsius = []
        self.fahrenheit = []
        
    def getTime(self):
        return self.time
    
    def getCelsius(self):
        return self.celsius
    
    def getFahrenheit(self):
        return self.fahrenheit
    
    def addReading(self, time, fahr, cels):
        self.time.append(time);
        self.fahrenheit.append(float(fahr))
        self.celsius.append(float(cels))                            
        