'''
Plots the incoming sensor data in real time, and provides a means of 
showing historical data as well
'''

from SensorClasses import SensorData
from tabulate import tabulate
import os

# Gets the data from the specified filename, and places it in a dictionary of SensorData
def getFileData(filename):
    data = {};
    sensData = open(filename, 'r');
    for line in sensData:
        chunks = line.split(" ")
        chunks[3] = chunks[3].strip(); # Remove the newline
        if chunks[1] in data:
            data[chunks[1]].addReading(chunks[0], chunks[2], chunks[3])
        else:
            toAdd = SensorData();
            toAdd.addReading(chunks[0], chunks[2], chunks[3])
            data[chunks[1]] = toAdd;  
        
    return data

# Change this to wherever the spot data file is actually located 
filename = 'C:\Users\soptnrs\Documents\Development\School\EC544\challenge-2\MATLAB-Scripts\spotData.txt'

# Get the current file size
size = os.stat(filename).st_size

# Get the data
currentData = getFileData(filename)

# As a test, print out the mote, and then all data right now
for mote, data in currentData.iteritems():
    values = []
    values.append(data.getTime())
    values.append(data.getCelsius())
    values.append(data.getFahrenheit())
    values = zip(*values);
    
    print 'Mote data for sensor {}'.format(mote)
    hds = ["Time", "Fahrenheit", "Celsius"]
    print tabulate(values, headers=hds, tablefmt="grid", floatfmt=".4f")
    print '\n'
    
    



    