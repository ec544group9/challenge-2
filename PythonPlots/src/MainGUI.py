'''
Created on Sep 20, 2013

@author: Raphy
'''

import wx
import matplotlib
import random
matplotlib.use('WXAgg')
 
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
 
# Declare the GUI elements for the historical graph
class PlottingFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self,parent, title=title, size=(800,600))
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Bind some callbacks
        self.Bind(wx.EVT_CLOSE, self.onCloseWindow)
         
        # Set up the control panels
        self.controlPanel = UpdatePanels(self)
         
        # Set up the plotting panels and the sizer
        self.plotPanels = PlottingPanel(self)
        self.plotPanels.drawHistorical(-1)
        self.plotPanels.drawInitialRT()

        self.sizer.Add(self.plotPanels, 1, wx.EXPAND)
        self.sizer.Add(self.controlPanel, 1, wx.EXPAND)
        
        self.SetSizer(self.sizer)
        
        # Set up the timer for updates
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.updateRTPlot, self.timer)
        self.timer.Start(1000)
        self.Show()
        
    # updates the plots 
    def updateRTPlot(self, args):
        self.plotPanels.updateRealTime()
        
    def updateHistorical(self, N):
        self.plotPanels.drawHistorical(N)
        
    def onCloseWindow(self, args):
        self.Destroy()
        
    def onUpdatePressed(self, args):
        count = self.controlPanel.textbox.Value
        count = int(count)
        if (count <= 0):
            return
        
        self.updateHistorical(count)
        

# The panel for the controls 
class UpdatePanels(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL);
        
        self.textbox = wx.TextCtrl(self, size=(200,-1), style=wx.TE_PROCESS_ENTER)      
        self.drawbutton = wx.Button(self, -1, "Update Historical")
        self.Bind(wx.EVT_BUTTON, parent.onUpdatePressed, self.drawbutton)
        
        self.sizer.Add(self.textbox, 1, wx.CENTER)
        self.sizer.Add(self.drawbutton, 1, wx.CENTER)
        
        self.SetSizer(self.sizer)
 
# The panels that show the graphs
class PlottingPanel(wx.Panel):
    # Parent is the frame object in which it exists
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = Figure()
        self.historicalAxes = self.figure.add_subplot(211)
        self.realtimeAxes = self.figure.add_subplot(212)
        
        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(self.sizer)
        self.Fit()
        
        self.times = []
        self.data = []
 
    # Gets the requested state and displays it 
    def drawHistorical(self, N):
        self.historicalAxes.clear()
        if (N == -1):
            # Indicates that we should draw all the data
            self.historicalAxes.plot(self.times, self.data)
        else:
            # Draw the latest N data points
            if (len(self.times) < N):
                t = self.times
                s = self.data
            else:
                t = self.times[-N:]
                s = self.data[-N:]

            self.historicalAxes.plot(t, s)
        self.canvas.draw();
            
    def drawInitialRT(self):
        self.realtimeAxes.plot([],[])
        self.canvas.draw()
    
    def updateRealTime(self):
        self.updateData()
        self.realtimeAxes.clear()    
        self.realtimeAxes.plot(self.times, self.data)
        self.canvas.draw();
        
    def updateData(self):
        if (len(self.times) == 0):
            self.times = [0]
        else:
            self.times.append(self.times[len(self.times) - 1] + 1)
            
        self.data.append(random.randint(50,100))

# The function for the realtime view of the data

# Initialization of all the elements
if __name__ == '__main__':
    app = wx.App(False)
    frame = PlottingFrame(None, "Historical View")
    app.MainLoop()

    print "I escaped the loop, time to kill my other process AHAHAHAHAHAHA"