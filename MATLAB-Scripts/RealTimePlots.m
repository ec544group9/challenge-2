function varargout = RealTimePlots(varargin)
    % REALTIMEPLOTS MATLAB code for RealTimePlots.fig
    %      REALTIMEPLOTS, by itself, creates a new REALTIMEPLOTS or raises the existing
    %      singleton*.
    %
    %      H = REALTIMEPLOTS returns the handle to a new REALTIMEPLOTS or the handle to
    %      the existing singleton*.
    %
    %      REALTIMEPLOTS('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in REALTIMEPLOTS.M with the given input arguments.
    %
    %      REALTIMEPLOTS('Property','Value',...) creates a new REALTIMEPLOTS or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before RealTimePlots_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to RealTimePlots_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES

    % Edit the above text to modify the response to help RealTimePlots

    % Last Modified by GUIDE v2.5 25-Sep-2013 14:35:36

    %% Initialization Code
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @RealTimePlots_OpeningFcn, ...
                       'gui_OutputFcn',  @RealTimePlots_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT

end

%% Function Callbacks

% --- Executes just before RealTimePlots is made visible.
function RealTimePlots_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to RealTimePlots (see VARARGIN)
    % Choose default command line output for RealTimePlots
    handles.output = hObject;
    handles.num_point = -1;

    % Update handles structure
    guidata(hObject, handles);

    % Set up a timer event for the GUI

    % UIWAIT makes RealTimePlots wait for user response (see UIRESUME)
    % uiwait(handles.figure1);

    handles.updateTimer = timer('TimerFcn', @(h, ev) updateGraphs(h, ev, hObject), 'ExecutionMode','FixedRate','Period',1);
    start(handles.updateTimer);
end

function updateGraphs(h, ev, hObject)
    %read the data from text file
    [col1 col2 col3 col4] = textread('spotData.txt', '%s %s %f %f', 'delimiter', ' ');

    number=unique(col2); %name of sensors
    len=length(number);  %number of sensors
    
    %The format for the date
    formatIn = 'yyyymmddHHMMSS';

    %for every sensor,create 3 arrays to store tempf,tempc and time
    for i=1:length(number)
        h=1;
        for j=1:length(col2)

            if strcmp(col2(j), number(i))

             datenumber = datenum(col1(j), formatIn);
             eval(['tempf' int2str(i) '(h)=col3(j);']);
             eval(['tempc' int2str(i) '(h)=col4(j);']);
             eval(['time' int2str(i) '(h)=datenumber;']);

              h=h+1;
            end

        end
    end



    for e=1:len

         eval(['resultf(e)=tempf' int2str(e) '(end);']);
         eval(['resultc(e)=tempc' int2str(e) '(end);']);
         eval(['resulttime(e)=time' int2str(e) '(end);']);

    end

    % Display the real time results on axes 3 (celsius) and axes 4
    % (fahrenheit)
    handles = guidata(hObject);
    fullCommand = 'plot(handles.axes3';
    full='';
    start = 0;
    for ii = 1:length(number)
       len = eval(['length(tempc' int2str(ii) ')']);
       if len <= 10
           start = 1;
       else
           start = len - 10;
       end
       cur = strcat(', time', int2str(ii), '(', int2str(start) ,':end), tempc', int2str(ii), '(', int2str(start) ,':end)');
       fullCommand = strcat(fullCommand, cur);
       
       lecur=strcat('''','sensor',int2str(ii),'''',',');
       full=strcat(full,lecur);
    end
    
    fullCommand = strcat(fullCommand, ')');
    set(handles.axes3, 'visible', 'off');
    eval(fullCommand);
    
    datetick(handles.axes3, 'x', 'mmm-dd-yy HH:MM:SS');
    set(handles.axes3,'XGrid','on');
    title(handles.axes3,'Celsius-Real time','FontSize',20);
    set(handles.axes3,'Fontsize',12)
    xlabel(handles.axes3,'Time(HHMMSS)');
    ylabel(handles.axes3,'Celsius')
    
    full = strcat('legend(handles.axes3, ', full, '-1)');
    eval(full);
    set(handles.axes3, 'visible', 'on');
    
%      x_formatstring = '%3.0f';
% 
%     xtick = get(handles.axes3, 'xtick');
%     for i = 1:length(xtick)
%         xticklabel{i} = sprintf(x_formatstring, xtick(i));
%     end
%     set(handles.axes3, 'xticklabel', xticklabel)
%   
    fullCommand = 'plot(handles.axes4';
    full='';
    for ii = 1:length(number)
       len = eval(['length(tempc' int2str(ii) ')']);
       if len <= 10
           start = 1;
       else
           start = len - 10;
       end
       cur = strcat(', time', int2str(ii), '(', int2str(start) ,':end), tempf', int2str(ii), '(', int2str(start) ,':end)');
       fullCommand = strcat(fullCommand, cur);
       
       lecur=strcat('''','sensor',int2str(ii),'''',',');
       full=strcat(full,lecur);
       
    end
    fullCommand = strcat(fullCommand, ')');
    
    eval(fullCommand);
    datetick(handles.axes4, 'x', 'mmm-dd-yy HH:MM:SS');
    set(handles.axes4,'XGrid','on');
    title(handles.axes4,'Fahrenheit-Real time','FontSize',20);
    set(handles.axes4,'Fontsize',12)
    xlabel(handles.axes4,'Time(HHMMSS)');
    ylabel(handles.axes4,'Fahrenheit')
    
    full = strcat('legend(handles.axes4, ', full, '-1)');
    set(handles.axes4, 'visible', 'on');
    eval(full);
    
    drawnow;
%      x_formatstring = '%3.0f';
% 
%     xtick = get(handles.axes4, 'xtick');
%     for i = 1:length(xtick)
%         xticklabel{i} = sprintf(x_formatstring, xtick(i));
%     end
%     set(handles.axes4, 'xticklabel', xticklabel)

end

% --- Outputs from this function are returned to the command line.
function varargout = RealTimePlots_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;
end

% --- Updates the historical axes
function pushbutton1_Callback(hObject, eventdata, handles)
    % hObject    handle to pushbutton1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
     %read the data from text file

    [col1 col2 col3 col4] = textread('spotData.txt', '%s %s %f %f', 'delimiter', ' ');

    number=unique(col2); %name of sensors
    len=length(number);  %number of sensors
    
    %The format for the date
    formatIn = 'yyyymmddHHMMSS';

    %for every sensor,create 3 arrays to store tempf,tempc and time
    for i=1:length(number)
        h=1;
        for j=1:length(col2)

            if strcmp(col2(j), number(i))

             datenumber = datenum(col1(j), formatIn);
             eval(['tempf' int2str(i) '(h)=col3(j);']);
             eval(['tempc' int2str(i) '(h)=col4(j);']);
             eval(['time' int2str(i) '(h)=datenumber;']);

             h=h+1;
            end

        end
    end



    for e=1:len

         eval(['resultf(e)=tempf' int2str(e) '(end);']);
         eval(['resultc(e)=tempc' int2str(e) '(end);']);
         eval(['resulttime(e)=time' int2str(e) '(end);']);

    end
    
    handles = guidata(hObject);
    
    fullCommand = 'plot(handles.axes1';
    full='';
    
    showCount = get(handles.edit1, 'String');
    showCount = str2num(showCount);
    if (showCount == [])
        showCount = -1;
    end
    
    for ii = 1:length(number)
       len = eval(['length(tempc' int2str(ii) ')']);
       if (showCount > len || showCount == -1)
           showCount = len;
       end
       cur = strcat(', time', int2str(ii), '((end - ', int2str(showCount), ' +1):end), tempc', int2str(ii), '((end - ', int2str(showCount), ' +1):end)');
       fullCommand = strcat(fullCommand, cur);
       
       lecur=strcat('''','sensor',int2str(ii),'''',',');
       full=strcat(full,lecur);
    end
    fullCommand = strcat(fullCommand, ')');
    eval(fullCommand);
    
    datetick(handles.axes1, 'x', 'mmm-dd-yy HH:MM:SS');
    set(handles.axes1,'XGrid','on');
    title(handles.axes1,'Celsius-Historical time','FontSize',20);
    set(handles.axes1,'Fontsize',12)
    xlabel(handles.axes1,'Time(HHMMSS)');
    ylabel(handles.axes1,'Celsius')
    
    full = strcat('legend(handles.axes1, ', full, '-1)');
    eval(full);
%     
%      x_formatstring = '%3.0f';
% 
%     xtick = get(handles.axes1, 'xtick');
%     for i = 1:length(xtick)
%         xticklabel{i} = sprintf(x_formatstring, xtick(i));
%     end
%     set(handles.axes1, 'xticklabel', xticklabel)
    
    
    

    fullCommand = 'plot(handles.axes2';
    full='';
    for ii = 1:length(number)
       len = eval(['length(tempc' int2str(ii) ')']);
       if (showCount > len || showCount == -1)
           showCount = len;
       end
       cur = strcat(', time', int2str(ii), '((end - ', int2str(showCount), ' +1):end), tempf', int2str(ii), '((end - ', int2str(showCount), ' +1):end)');
       fullCommand = strcat(fullCommand, cur);
       
       lecur=strcat('''','sensor',int2str(ii),'''',',');
       full=strcat(full,lecur);
    end
    fullCommand = strcat(fullCommand, ')');
    eval(fullCommand);
    
    datetick(handles.axes2, 'x', 'mmm-dd-yy HH:MM:SS');
    set(handles.axes2,'XGrid','on');
    title(handles.axes2,'Fahrenheit-Historical time','FontSize',20);
    set(handles.axes2,'Fontsize',12)
    xlabel(handles.axes2,'Time');
    ylabel(handles.axes2,'Fahrenheit');
    
    full = strcat('legend(handles.axes2,', full, '-1)');
    eval(full);
%     
%      x_formatstring = '%3.0f';
% 
%     xtick = get(handles.axes2, 'xtick');
%     for i = 1:length(xtick)
%         xticklabel{i} = sprintf(x_formatstring, xtick(i));
%     end
%     set(handles.axes2, 'xticklabel', xticklabel)

end

% Makes sure that every thing closes and the loop stops
function closereq()
    stop(handles.updateTimer)
end

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
    % hObject    handle to CloseMenuItem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                         ['Close ' get(handles.figure1,'Name') '...'],...
                         'Yes','No','Yes');
    if strcmp(selection,'No')
        return;
    end

    delete(handles.figure1)
    figureOpen = false;

end

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to popupmenu1 (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
         set(hObject,'BackgroundColor','white');
    end

    set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});
end


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.

user_string = get(hObject,'String');
%handles.num_point = int32(user_string);

handles.num_point = str2num(user_string);
if (handles.num_point == [])
    handles.num_point = -1;
end

end


function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
