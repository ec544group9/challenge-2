Challenge 2 - Group 9

The code for our challenge 2 is heavily based off of our challenge 1 code

Challenge1-OnSpot folder is the netbeans project that contains the code that is sent to each
mote for taking temperature readings.  Challenge1-OnDesktop contains the basestation code
for retrieving all sensor data from the motes, and sending them to the desktop for saving to 
a file.  The date data is written to a file using yyyymmddHHMMSS format, so that data for multiple
days, and even years, can be tracked if needed.

The MATLAB-Scripts folder contains the scripts we used for display.  In particular, for challenge 2 the
key files are:
	RealTimePlots.m
	RealTimePlots.fig

Running RealTimePlots opens a GUI with 4 figures,  the right two figures are real time display as the 
text file gets updated.  This updates every second to get the most recent data.  The left two plots are
historical, and using the text box and button at the bottom of the GUI, the last N number of points can
be displayed.

Special note, to run properly, in the OnDesktop Java program, the line:

File file = new File("/Users/Raphy/Documents/School/Fall 2013/EC544/Challenge 1/MATLAB-Scripts/spotData.txt");

must be changed to

File file = new File("/Path/to/challenge/folder/MATLAB-Scripts/spotData.txt");

where the path to challenge folder is replaced with the path to the root directory of the project on your
computer.


Along with our solution, the Python-Scripts folders contains prototypes for the same implementation, but made
using Python.  This would have been our eventual goal, to improve some of the shortcomings that we saw occurred
while using MATLAB
